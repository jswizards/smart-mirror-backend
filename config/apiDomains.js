module.exports = {
    kairosAPI: 'https://api.kairos.com',
    weatherAPI: 'https://api.openweathermap.org',
    eventbriteAPI: 'https://www.eventbriteapi.com/v3',
    googleAPI: 'https://maps.googleapis.com',
    mapQuestAPI: 'http://www.mapquestapi.com',
    fandangoAPI: 'http://api.fandango.com'
};
