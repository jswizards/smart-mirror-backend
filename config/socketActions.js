//ALL possible socket actions that we emitting

module.exports = {
    MOTION_DETECTED:            'RASPBERRY:MOTION_DETECTED',
    PERSON_DETECTED:            'RASPBERRY:PERSON_DETECTED',
    PERSON_LEFT:                'RASPBERRY:PERSON_LEFT',
    GOOGLE_PUSH_NOTIFICATION:   'update google calendar events'
};
