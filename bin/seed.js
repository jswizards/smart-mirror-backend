const mongoose = require('mongoose');//get mongoose
require('dotenv').config();
mongoose.connect(process.env.MONGODB_URI);

const Quote = require('../models/Quote.js');

const quotes =[
  {
    quote: 'The best and most beautiful things in the world cannot be seen or even touched - they must be felt with the heart.',
    author: 'Helen Keller',
  },
  {
    quote: 'Keep love in your heart. A life without it is like a sunless garden when the flowers are dead.',
    author: 'Oscar Wilde',
  },
  {
    quote: 'It is during our darkest moments that we must focus to see the light.',
    author: 'Aristotle',
  },
  {
    quote: 'Try to be a rainbow in someones cloud.',
    author: 'Maya Angelou',
  },
  {
    quote: `Find a place inside where there's joy, and the joy will burn out the pain.`,
    author: 'Joseph Campbell',
  },
  {
    quote: `Nothing is impossible, the word itself says 'I'm possible'`    ,
    author: 'Audrey Hepburn',
  },
];
Quote.create(quotes, (err,docs)=>{
  if(err) throw err;
  docs.forEach((oneDoc)=>{
      console.log(`Inserted ${oneDoc.quote} ${oneDoc._id}`);
  });
  mongoose.disconnect();
});