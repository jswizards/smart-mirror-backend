const mongoose = require('mongoose');//get mongoose
require('dotenv').config();
mongoose.connect(process.env.MONGODB_URI);

const User = require('../models/UserModel.js');

const docs =[
  {
    firstName: 'Oleh',
    lastName: 'Kolinko',
    DOB: new Date('03-26-1994'),
    username: 'oleh.kolinko',
    subjectId: 'olehkolinko',
    password: '',
    email: 'oleh.kolinko@gmail.com',
    settings : {
        cryptoPrice : { 
            show: true,
            coins: ['BTC']
        },
    }
  },
  {
    firstName: 'Ihor',
    lastName: 'Bodnarchuk',
    DOB: new Date('01-28-1994'),
    username: 'ihorbond',
    subjectId: 'ihorbond',
    password: '',
    email: 'ihorbond@gmail.com@gmail.com',
    settings : {
        cryptoPrice : { 
            show: true,
            coins: ['BTC','ETH']
        },
    }
  }
];
User.create(docs, (err,docs)=>{
  if(err) throw err;
  docs.forEach((oneDoc)=>{
      console.log(`Inserted ${oneDoc.quote} ${oneDoc._id}`);
  });
  mongoose.disconnect();
});