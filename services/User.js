const UserModel = require('../models/UserModel.js');

class User{

    //Get User by subjectId:
    static async getBySubjectId(subjectId){

        try {
            //Get user from DB:
            let user = await UserModel.findOne({subjectId: subjectId});

            if(!user)
                return null; //We did not find user

            //Don't send password 
            user.password = undefined; 
            
            //send back user
            return user;

        } catch (error) {
            console.log(error)
        }
        
    }


}

module.exports = User;