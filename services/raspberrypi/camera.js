//const PiCamera  = require('pi-camera');
const fs = require('fs');
const RPiCamera = require('node-raspistill').Raspistill;
//const outputDir = `${ __dirname }/media/pictures`;
//const shell = require('shelljs');

class Camera {
  // constructor(mode) {
  //   this.mode = mode;
  // }

  static async takePic() {
    //const timestamp = new Date().getTime();
    //const path = `${outputDir}/${timestamp}.jpg`

    // shell.exec(`raspistill -vf -hf -o ${path}`);
    //
    // const base64Pic = await this.base64Encode(path);
    //
    //   this.deleteFile(path);
    //
    //   return base64Pic;
    // const myCamera = new PiCamera({
    //   mode: 'photo',
    //   output: path,
    //   nopreview: true
    // });

    let camera = new RPiCamera({
      noFileSave: true,
      verticalFlip: true,
      horizontalFlip: true,
      noPreview: true,
      width:480,
      height:640,
      time: 0
    })

    try {
      const photo = await camera.takePhoto();
      console.log(photo);
      //const base64Pic = await this.base64Encode(photo);

      return photo.toString('base64');
    } catch (error) {
      console.log(error)
    }
  }

  static deleteFile(path){
    fs.unlink(path);
  }
}

module.exports = Camera;
