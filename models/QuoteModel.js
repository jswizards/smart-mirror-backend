
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuoteSchema = new Schema({
    quote: { type: String, required: true },
    author: { type: String, required: true },
},{
    timestamps: true
}
);

const Quote = mongoose.model('Quote', QuoteSchema);

module.exports = Quote;