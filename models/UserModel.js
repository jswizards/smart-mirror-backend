const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    firstName: String,
    lastName: String,
    DOB: Date,
    username: { type: String, required: true },
    subjectId: String,
    password: String,
    email: { type: String, required: true },
    settings : {
        currentWeather: {
            show: { type: Boolean, default: true }
        },
        currentCity: {
            show: { type: Boolean, default: true }
        },
        commute: {
            show: { type: Boolean, default: true }
        },
        weatherForecast: {
            show: { type: Boolean, default: true }
        },
        quote: {
            show: { type: Boolean, default: true }
        },
        cryptoPrice: {
            show: { type: Boolean, default: true },
            coins: [String]
        },
        events: {
            show: { type: Boolean, default: true }
        },
        eventsNearby: {
            show: { type: Boolean, default: true }
        },
        alarmClock: {
            show: { type: Boolean, default: true },
            alarms : [{
                time: String,
                daysOfTheWeek: [String]
            }]
        },
        

        location: String,
        tokens: {

        },
        raspberryPiMACAdress: String,
    }
},{
    timestamps: true
});

const User = mongoose.model('User', UserSchema);

module.exports = User;