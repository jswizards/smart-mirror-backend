const express      = require('express');
const path         = require('path');
const cors         = require('cors');
const socketIO 	   = require('socket.io')
const mongoose     = require('mongoose');
const morgan       = require('morgan');
const bodyParser   = require('body-parser');
const http         = require('http');
require('dotenv').config();

const port = 4200;
const app = express();

//Connect to DB
mongoose.connect(process.env.MONGODB_URI);

//Setup Cors
const corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(cors());

//Default view engine
app.set('view engine', 'html');

//Setup socket IO
const server = http.createServer(app);
const io = socketIO(server);
io.set('origins', '*:*');

io.on('connection', socket => {
	console.log('Socket.io: connected')

	socket.on('disconnect', () => {
		console.log('Socket.io: disconnected')
	})
})

app.locals.title = "Magic Mirror";

app.use(morgan('tiny')); //Logger
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));
//app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// app.get('/google32ce6d5fcdf0adad.html', function(req, res) {
//     res.sendFile(path.join(__dirname + '/google32ce6d5fcdf0adad.html'));
// });


//All rest api:
const rest = require('./routes/rest')(io);
app.use('/rest', rest);

//hardware
const hardware = require('./routes/hardwareApi')(io);
app.use('/raspberry', hardware);

server.listen(port, () => console.log(`listening on ${port}`));

// catch 404 and forward to error handler
app.use((req, res, next) => {
	res.sendStatus(404)
});

module.exports = app;
