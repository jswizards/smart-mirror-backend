const express = require('express');
const router  = express.Router();

module.exports = (io) => {

    //QuotesService
    const user = require('./rest/user.js')(io);
    router.use('/user', user);

    //QuotesService
    const quotes = require('./rest/quotes.js');
    router.use('/quote', quotes);

    //CryptoService
    const crypto = require('./rest/cryptoStockPrices.js');
    router.use('/crypto', crypto);

    //Commute
    const commute = require('./rest/commute.js');
    router.use('/commute', commute);

    //weather
    const weather = require('./rest/weather.js');
    router.use('/weather', weather);

    //faceRecognition
    const faceRecognition = require('./rest/faceRecognition.js');
    router.use('/face-recognition', faceRecognition);

    //currentLocation
    const currentLocation = require('./rest/currentLocation.js');
    router.use('/current-city', currentLocation);

    //eventsNearby
    const eventsNearby = require('./rest/eventsNearby.js');
    router.use('/events-nearby', eventsNearby);

    //googleCalendarEvents
    const googleCalendar = require('./rest/googleCalendar.js');
    router.use('/google', googleCalendar);

    //googleCalendarNotifications
    const googleCalendarNotifications = require('./rest/googleCalendarNotifications.js')(io);
    router.use('/google', googleCalendarNotifications);

    //news
    const news = require('./rest/news.js')(io);
    router.use('/news', news);

    return router;
}
