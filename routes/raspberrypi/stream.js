const express = require('express');
const router  = express.Router();
const socketActions = require('../../config/socketActions.js');
const shell = require('shelljs');
const command = 'sudo python ~/code/smart-mirror/start_stream.py';
const childProcess = require('child_process');
const streamPort = 8000;

module.exports = (io) => {
	
	const childOptions = {
		shell: false
	};
	
	let stream = null;
	
	let childCallback = (err, stdout, stderr) => {
		if(err) console.log(err); return;
		if(stderr) console.log(stderr); return;
		console.log(stdout);
	};
	
    router.get('/start', (req, res, next)=>{
		if(stream === null || stream.killed) {
			stream = childProcess.exec(command, childOptions, childCallback);
		}
		res.sendStatus(200);
    });
    
    router.get('/stop', (req, res, next)=>{
		onExit();
		res.sendStatus(200);
    });
    
    let onExit = () => {
		if(stream != null && !stream.killed) {
			//console.log("killing stream process");
			
			//process.kill(-stream.pid);
			stream.kill();
			//let killCommand = `sudo fuser -k {streamPort}/tcp`;
			console.log("stream killed: " + stream.killed);
			//console.log(killCommand);
			//let exitCode = shell.exec(killCommand).code;
			//console.log("Exit code: " + exitCode);
			//stream = null;
			//console.log("Stream exit code = " + exitCode);
			//process.exit(0);
		}
	}
    
    process.on('SIGINT', onExit);
    process.on('exit', onExit);
    return router;
}
