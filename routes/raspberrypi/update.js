const express = require('express');
const router  = express.Router();
const socketActions = require('../../config/socketActions.js');
const shell = require('shelljs');
const path = '~/code/smart-mirror/pull.sh';

module.exports = (io) => {

    router.get('/', async (req, res, next)=>{
      console.log("repo update detected");
        //shell.cd(path);

        if (shell.exec(path).code !== 0) {
          res.sendStatus(500);
        }
        else {
          res.sendStatus(200);
        }
    })
    return router;
}
