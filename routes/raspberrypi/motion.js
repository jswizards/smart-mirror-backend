const express = require('express');
const router  = express.Router();
const socketActions = require('../../config/socketActions.js');
const Camera = require('../../services/raspberrypi/camera.js');
const Kairos = require('../../services/kairos.js');
const User = require('../../services/User.js');
const LZString = require('lz-string');

module.exports = (io) => {
  const kairos = new Kairos();
  router.post('/present', async (req, res, next)=>{
    //motions detected -> try to recognise the face
    io.emit(socketActions.MOTION_DETECTED);

    try {
      //Take base64Pic:
      //let base64Pic = await Camera.takePic();
      let base64Pic = req.body.photo;
      //console.log("orig size " + base64Pic.length);
      //console.log("compressed " + LZString.compress(base64Pic).length);
      //console.log("pic size" + Buffer.byteLength(base64Pic, 'utf8') + 'bytes');
      console.time('Kairos');

      //send pic to kairos and get all candidates
      let kairosResponse = await kairos.identity('recognize', base64Pic, '');

		  console.timeEnd('Kairos');

      if(kairosResponse.status === 200) {
        //Find bestMatch:
        const candidates = kairosResponse.data.images[0].candidates;
        let bestCandidate = candidates[0];
        candidates.forEach(oneCand => {
          if(oneCand.confidence > bestCandidate.confidence) {
            bestCandidate = oneCand;
          }
        });

        //Get user:
        //console.log("Cand " + bestCandidate);
        const user = await User.getBySubjectId(bestCandidate.subject_id);
		    //console.log(user)
        //Success:
        io.emit(socketActions.PERSON_DETECTED, {status: kairosResponse.status, user: user, msg: "Found user"});
        res.sendStatus(200);
      }
        //Kairos failed:
        else {
			res.sendStatus(kairosResponse.status);
		}

    } catch (error) {
      console.log(error);
      res.sendStatus(500);
    }

  });

  router.post('/left',(req, res, next)=>{
    //Person left -> notify frontend
    io.emit(socketActions.PERSON_LEFT, {status: '', user: null, msg: `Person left`});
    res.sendStatus(200);
  });

  router.get('/start', async (req, res, next)=>{
    const command = 'python ~/code/smart-mirror/start_motion_detection.py';
    console.log("motion detection start request detected");
      //shell.cd(path);

      if (shell.exec(command).code !== 0) {
        res.sendStatus(500);
      }
      else {
        res.sendStatus(200);
      }
  })

  return router;
}
