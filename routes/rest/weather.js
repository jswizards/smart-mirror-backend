const request = require('request');
const express = require('express');
const router  = express.Router();
const apiDomains = require('../../config/apiDomains');

const weatherAPI = apiDomains.weatherAPI;
const weatherKey 	= process.env.OWM_KEY;


router.get('/current/:lat/:lon', (req, res, next) => {
		let lat = req.params.lat;
		let lon = req.params.lon;
		let uri = `${weatherAPI}/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${weatherKey}`;
		request.get(uri, (err, response, body) => {
			if(err) {
				console.log(err);
			}
			if(response.statusCode === 200) {
				//console.log(body);
				let json = JSON.parse(body);
				res.json({weather: json});
			}
	});
	});

	router.get('/forecast/:lat/:lon', (req, res, next) => {
		let lat = req.params.lat;
		let lon = req.params.lon;
		let uri = `${weatherAPI}/data/2.5/forecast?lat=${lat}&lon=${lon}&APPID=${weatherKey}`;
		request.get(uri, (err, response, body) => {
			if(err) {
				console.log(err);
			}
			if(response.statusCode === 200) {
				//console.log(body);
				let json = JSON.parse(body);
				res.json({forecast: json.list});
			}
	});
	});

module.exports = router;
