module.exports = (io) => {

const request = require('request');
const express = require('express');
const router  = express.Router();
const apiDomains = require('../../config/apiDomains');
const socketActions = require('../../config/socketActions.js');

router.post('/notifications', (req, res, next) => {
		io.emit(socketActions.GOOGLE_PUSH_NOTIFICATION);
		res.sendStatus(200);
	});
  return router;
}
