const express = require('express');
const router  = express.Router();

const NewsAPI = require('newsapi');
const newsapi = new NewsAPI(process.env.NEWS_KEY);

module.exports = (io) => {

	filterNews = (data) => {
    const count = data.totalResults;
    let news = [];
    data.articles.forEach(article => {
      news.push({
        source: article.source.name,
        title: article.title,
        time: article.publishedAt,
        url: article.url
      });
    });
    return news;
  }

  router.post('/', (req, res, next) => {
      //console.log(req.body);
      newsapi.v2.topHeadlines({
        sources: req.body.sources,
        q: req.body.keywords,
        category: req.body.category,
        language: req.body.language,
        country: req.body.country,
        //from: '2017-12-01',
        //to: '2017-12-12',
        sortBy: 'relevancy'
      }).then(response => {
      //console.log(response);
      if(response.status === 'ok') {
          const news = filterNews(response);
          res.send({status: 200, news: news, err: null});
      }
      else {
          res.send({status: 500, news: null, err: response.message});
      }
    }).catch(err => {
      res.send({status: 500, news: null, err: err});
    });

  })

  return router;
}
