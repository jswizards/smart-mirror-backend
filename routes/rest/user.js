const express = require('express');
const router = express.Router();
const User = require('../../services/User.js');

module.exports = (io)=>{

    //Get User by subjectId:
    router.get('/subject-id/:id', async function(req,res,next) {
        const subjectId = req.params.id;

        User.getBySubjectId(subjectId)
            .then((user) => {

                if(!user) res.sendStatus(204)
                res.json({user});
            })
            .catch(err => res.sendStatus(500));
    });

    return router;
}