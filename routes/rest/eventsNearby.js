const request = require('request');
const express = require('express');
const router  = express.Router();
const apiDomains = require('../../config/apiDomains');

const eventbriteAPI = apiDomains.eventbriteAPI;
const eventbriteToken = process.env.EVENTBRITE_TOKEN;

router.post('/', (req, res, next) => {
		//console.log(req.body);
		let dist = req.body.range + req.body.units;;
		let lat = req.body.lat;
		let lon = req.body.lon;
		let sortBy = req.body.sortBy;
		let uri = `${eventbriteAPI}/events/search/?sort_by=${sortBy}&location.latitude=${lat}&location.longitude=${lon}&location.within=${dist}&start_date.keyword=today&token=${eventbriteToken}`;
		request.get(uri, (err, response, body) => {
			if(err) {
				console.log(err);
			}
			if(response.statusCode === 200) {
				let filteredEvents = [];
				let json = JSON.parse(body)
				json.events.forEach(oneEvent => {
					let eventStart = new Date(oneEvent.start.utc).getTime();
					let hourAway = new Date().getTime() < eventStart - 3600000; // 3600000ms = 1hr
					if(hourAway && !oneEvent.online_event) {
						filteredEvents.push(oneEvent);
					}
				});
				res.json({events: filteredEvents});
			}
	});
	});

module.exports = router;
