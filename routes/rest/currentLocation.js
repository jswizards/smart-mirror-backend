const request = require('request');
const express = require('express');
const router  = express.Router();
const apiDomains = require('../../config/apiDomains');

const googleAPI = apiDomains.googleAPI;

router.get('/:lat/:lon', (req, res, next) => {
	console.log("received current location request");
		const lat = req.params.lat;
		const lon = req.params.lon;
		let uri = `${googleAPI}/maps/api/geocode/json?latlng=${lat},${lon}&sensor=true`;
		request.get(uri, (err, response, body) => {
			if(err) {
				res.send("Service temporarily unavailable");
				console.log(err);
			}
			if(response.statusCode === 200) {
				//console.log(body);
				let json = JSON.parse(body);
				try{
					res.send(json.results[0].address_components[3].long_name);
				}
				catch(err) {
					res.send("Service temporarily unavailable");
				}
			}
	});
	});

module.exports = router;
