const axios = require('axios');
const express = require('express');
const router  = express.Router();
const moment = require('moment')
const QuoteModel =  require('../../models/QuoteModel.js');


router.get('/', (req, res, next) => {

    //Count all records
    QuoteModel.count().exec( (err, count) => {

        // Get a random entry
        let random = Math.floor(Math.random() * count)
      
        // Again query all users but only fetch one offset by our random #
        QuoteModel.findOne().skip(random).exec(
           (err, quote) => {
            
            // send back random user
            res.json({quote})
          })
      })
});

// router.get('/', (req, res, next) => { //GET Quote of the day
    
//     //Using external API: https://theysaidso.com/api/
//     //They have api limit of 10 calls/hour
//     const quotesAPIurl = 'http://quotes.rest/qod.json?category=inspire';

//     //Get latest quote from db
//     Quote.findOne()
//         .sort({ field: 'asc', _id: -1 }).limit(1) //Get the latest
//         .exec( (err,doc) => {
//             if(err){
//                 console.log(err);
//                 res.sendStatus(500);
//             }
//             //Document found!
//             let quote = {
//                 quote:  doc.quote,
//                 author: doc.author,
//             }

//             //Check if latest quote is for today:
//             const quoteDate = moment(doc.date).format('YYYY-MM-DD');
//             const today 	= moment().format('YYYY-MM-DD');

//             if(today <= quoteDate){
//                 console.log('QUOTE: Sending back quote from our DB');
//                 res.json({quote}); // send today's quote
//             }else{
//                 //quote in DB is old -> need to request new one:
//                 axios.get(quotesAPIurl)
//                     .then(response=>{
//                         const parsedBody = JSON.parse(body);
//                         const quoteFromApi = parsedBody.contents.quotes[0];
                        
//                         //Save quote from API to DB
//                         let newQuote = new Quote({
//                             quote:  quoteFromApi.quote,
//                             author: quoteFromApi.author,
//                             date: new Date()
//                         });
//                         newQuote.save((err, doc)=>{
//                             if(err) console.log(err)
//                         })

//                         //Send response with fresh quote
//                         quote = {
//                             quote:  quoteFromApi.quote,
//                             author: quoteFromApi.author
//                         }
//                         console.log('QUOTE: Sending back fresh quote from API and saving to our DB');
//                         res.json({quote})
//                     })
//                     .catch(err=>{
//                         console.log(err);
//                         //Quote API Error -> send quote from DB
//                         console.log('QUOTE: Error with quote api, sending back quote from our DB');
//                         res.json({quote});
//                     })
//             }
//         })

// });


module.exports = router;