const express = require('express');
const router  = express.Router();
const fs = require('fs');
const readline = require('readline');
const google = require("googleapis");
const OAuth2Client = google.auth.OAuth2;
const TOKEN_PATH = './config/google_calendar_token.json';
const SECRET_PATH = './config/google_client_secret.json'

let json = {
	error: "",
	events: []
};

router.get('/events', (req, res, next) => {
  console.log("Received google cal events req")
	const SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];
	// let TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
	// 		process.env.USERPROFILE) + '/.credentials/';
	// let TOKEN_PATH = TOKEN_DIR + 'calendar-nodejs-quickstart.json';

	// Load client secrets from a local file.
	fs.readFile(SECRET_PATH, (err, content) => {
		//console.log("Content: " + content);
		if (err) {
			json.error = 'Error loading client secret file: ' + err;
			console.log('Error loading client secret file: ' + err);
			return;
		}
		// Authorize a client with the loaded credentials, then call the
		// Google Calendar API.
		authorize(JSON.parse(content), listEvents);
	});

	/**
	 * Create an OAuth2 client with the given credentials, and then execute the
	 * given callback function.
	 *
	 * @param {Object} credentials The authorization client credentials.
	 * @param {function} callback The callback to call with the authorized client.
	 */
	function authorize(credentials, callback) {
		let clientSecret = credentials.installed.client_secret;
		let clientId = credentials.installed.client_id;
		let redirectUrl = credentials.installed.redirect_uris[0];
		const oauth2Client = new OAuth2Client(clientId, clientSecret, redirectUrl);
		//getNewToken(oauth2Client, callback);
		// Check if we have previously stored a token.
		fs.readFile(TOKEN_PATH, (err, token) => {
	    if (err) return getAccessToken(oauth2Client, callback);
	    oauth2Client.setCredentials(JSON.parse(token));
	    callback(oauth2Client);
	  });
		// fs.readFile(TOKEN_PATH, function(err, token) {
		// 	//console.log(tokenExpired(TOKEN_PATH))
		// 	//if(tokenExpired(token)) {
		// 	if (err) {
		// 		getNewToken(oauth2Client, callback);
		// 	} else {
		// 		oauth2Client.credentials = JSON.parse(token);
		// 		callback(oauth2Client);
		// 	}
		// });
	}

	function tokenExpired(token) {
		let currentDate = new Date().getTime();
		let tokenExpired = true;
		// return fs.readFile(TOKEN_PATH, function(err, token) {
		// 	if(err) {
		// 		console.log("error reading bearer token");
		// 		return tokenExpired;
		// 	} else {
				let credentials = JSON.parse(token);
				tokenDate = credentials.expiry_date;
				console.log('tokenDate' + tokenDate);
				console.log('currentDate' + currentDate);
				tokenExpired = (tokenDate < currentDate) ? true : false;
				console.log("Token expired " + tokenExpired);
				return tokenExpired;
		// 	}
		// })
		//console.log("token exp " + tokenExpired);

	}

	/**
	 * Get and store new token after prompting for user authorization, and then
	 * execute the given callback with the authorized OAuth2 client.
	 *
	 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
	 * @param {getEventsCallback} callback The callback to call with the authorized
	 *     client.
	 */
	function getNewToken(oauth2Client, callback) {
		var authUrl = oauth2Client.generateAuthUrl({
			access_type: 'offline',
			scope: SCOPES
		});
		console.log('Authorize this app by visiting this url: ', authUrl);
		var rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		});
		rl.question('Enter the code from that page here: ', function(code) {
			rl.close();
			oauth2Client.getToken(code, function(err, token) {
				if (err) {
					console.log('Error while trying to retrieve access token', err);
					return;
				}
				oauth2Client.credentials = token;
				storeToken(token);
				callback(oauth2Client);
			});
		});
	}

	/**
	 * Store token to disk be used in later program executions.
	 *
	 * @param {Object} token The token to store to disk.
	 */
	function storeToken(token) {
		try {
			fs.mkdirSync(TOKEN_DIR);
		} catch (err) {
			if (err.code != 'EEXIST') {
				throw err;
			}
		}
		fs.writeFile(TOKEN_PATH, JSON.stringify(token));
		console.log('Token stored to ' + TOKEN_PATH);
	}

	/**
	 * Lists the next 10 events on the user's primary calendar.
	 *
	 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
	 */
	 function findDuplicate(arr) {
		const result = arr.filter((value, index, array) => array.indexOf(value.date) !== index);
		return result;
	}

	function listEvents(auth) {
		const calendar = google.calendar({version: 'v3', auth});
		calendar.events.list({
			calendarId: 'primary',
			timeMin: (new Date()).toISOString(),
			maxResults: 10,
			singleEvents: true,
			orderBy: 'startTime'
		}, function(err, response) {
			if (err) {
				console.log('The API returned an error: ' + err);
				return;
			}
			json.events = response.data.items;
			//console.log(response.data);



			res.send(json);
			// if (events.length == 0) {
			// 	console.log('No upcoming events found.');
			// } else {
			// 	console.log('Upcoming 10 events:');
			// 	for (var i = 0; i < events.length; i++) {
			// 		var event = events[i];
			// 		var start = event.start.dateTime || event.start.date;
			// 		console.log('%s - %s', start, event.summary);
			// 	}
			// }
		});
	}

});
//
module.exports = router;
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/calendar-nodejs-quickstart.json
