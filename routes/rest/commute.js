const request = require('request');
const express = require('express');
const router  = express.Router();
const querystring = require('querystring');
const apiDomains = require('../../config/apiDomains');

const mapQuestAPI = apiDomains.mapQuestAPI;
const mapQuestKey = process.env.MAP_QUEST_KEY;

router.post('/traffic', (req, res, next) => {
		const params = {
			key: mapQuestKey,
			boundingBox: req.body.boundingBox,
			filters: req.body.filters,
		};
		//console.log(params);
		const query = querystring.stringify(params);
		let traffic = {
			incidents: [],
			error: ''
		};

		let uri = `${mapQuestAPI}/traffic/v2/incidents?${query}`;
		request.get(uri, (err, response, body) => {
				if(err) {
					traffic.error = err;
				}
				if(response.statusCode === 200) {
					let json = JSON.parse(body);
					traffic.incidents = json.incidents;
					//console.log(body);
					res.send(traffic);
				}
				else {
					traffic.error = response.statusCode;
					res.send(traffic);
				}
			});
	});

	router.post('/', (req, res, next) => {
		//console.log("Commute req " + req.body);
		const params = {
			key: mapQuestKey,
			from: req.body.from,
			to: req.body.to,
			avoids: req.body.avoids
		};
		//console.log(params);
		const query = querystring.stringify(params);
		//console.log("commute query " + query);
		let commute = {
			time: 'Unavailable',
			distance: 'Unavailable',
			hasTolls: 'Unavailable',
			copyright: 'Unavailable',
			directions: 'Unavailable',
			boundingBox: 'Unavailable',
			error: ''
		};

		let uri = `${mapQuestAPI}/directions/v2/route?${query}`;
		request.get(uri, (err, response, body) => {
			  // console.log(response + "\n-----------------\n");
				// console.log(body);
				if(err) {
					commute.error = err;
				}
				if(response.statusCode === 200) {
					let json = JSON.parse(body);
					//console.log(body);
					commute.time = Math.round(json.route.realTime/60); //convert to mins
					commute.distance = json.route.distance;
					commute.hasTolls = json.route.hasTollRoad;
					commute.copyright = json.info.copyright;
					commute.directions = json.route.locations;
					commute.boundingBox = json.route.boundingBox;
					res.send(commute);
				}
				else {
					commute.error = response.statusCode;
					res.send(commute);
				}
			});
		});

module.exports = router;
