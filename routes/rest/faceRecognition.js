const request = require('request');
const express = require('express');
const router  = express.Router();
const apiDomains = require('../../config/apiDomains');
const Kairos = require('../../services/kairos.js');

//kairos enroll/verify/recognize based on action
	router.post('/identity', async (req, res, next) => {
		const action = req.body.action;
		const img = req.body.img;
		const subjectID = req.body.subjectID;
		let response = await Kairos.identity(action, img, subjectID);
		if(response.status === 200) {
			res.json(response.data)
		}
		else{
			res.sendStatus(response.status);
		}
	});

module.exports = router;
