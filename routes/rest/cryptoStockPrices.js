const axios = require('axios');
const express = require('express');
const router  = express.Router();


//send symbols that you want to get prices for, separated by coma
//example:
//  .../rest/cryptoPrices/BTC,ETH,LTC

router.get('/:symbols', (req, res, next) => { 
    let symbols = req.params.symbols;

    //using external api
    let apiURL = 'https://min-api.cryptocompare.com/data/pricemulti'
    
    apiURL += `?fsyms=${symbols}&tsyms=USD`

    axios.get(apiURL)
        .then( response => {
            res.json(response.data)
        })
        .catch( err => {
            console.log(err)
            res.sendStatus(500)
        })
})

module.exports = router;