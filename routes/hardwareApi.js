const express = require('express');
const router  = express.Router();

//All interaction with hardware/raspberry

module.exports = (io) => {

    //Motion detected
    const motion = require('./raspberrypi/motion.js')(io);
    router.use('/motion', motion);

    const update = require('./raspberrypi/update.js')(io);
    router.use('/update', update);

    const stream = require('./raspberrypi/stream.js')(io);
    router.use('/stream', stream);


    return router;
}
